// Import the authentication code for the plugin
import authService from './authService'

// This is a plugin that does two things:
// 1. It creates an AuthService instance and adds it as a global service to the Vue instance so that there is one
//    authentication service for all components in a Vue application
// 2. It adds a global mixin to Vue that attaches created and destroyed mixin methods to every component in the
//    application.  When created is called on a component, the mixin created gets called first and it checks to see
//    if the component being created has a method to handle login event (named 'handleLoginEvent').  If the component
//    has that method, that method is then attached to the AuthService (which is an EventEmitter) so that it gets
//    called anytime the AuthService emits a 'loginEvent'.  In the destroyed method, it removes that method from the
//    AuthService so it's no longer sent 'loginEvent' messages

// This is the required 'install' method that is needed for a object to be used as a plugin in Vue
export function install (Vue, options) {
  // Configure the authentication service using incoming options
  authService.configureService(options.domain, options.redirectUri, options.spaClientID, options.mobileClientID,
    options.mobilePackageID, options.audience, options.isMobile)

  // Register the AuthService on the global Vue object so it's available to all components in the application
  Vue.prototype.$auth = authService

  // Add the global mixing that adds created and destroyed methods that will be called before those same methods
  // on any component
  Vue.mixin({
    created () {
      // Check to see if the component has a handleLoginEvent method
      if (this.handleLoginEvent) {
        console.log('AuthPlugin created called and component has handleLoginEvent, adding as login event listener')
        // Register the method to the AuthService event emitter
        authService.addListener(authService.LOGIN_EVENT, this.handleLoginEvent)
        // Go ahead and tell the auth service to publish that status to make sure the newly connected listener
        // gets the current login status.
        authService.publishStatus()
      }
    },
    destroyed () {
      // Check to see if the component has a handleLoginEvent method
      if (this.handleLoginEvent) {
        console.log('AuthPlugin destroyed called and component has handleLoginEvent, removing login event listener')
        // Remote the method from the list of listeners on the AuthService
        authService.removeListener(authService.LOGIN_EVENT, this.handleLoginEvent)
      }
    }
  })
}

// Expose the service
export {
  authService
}

// Plugin definition
const plugin = {
  /* eslint-disable no-undef */
  version: VERSION,
  install
}
export default plugin

// Auto-install
let GlobalVue = null
if (typeof window !== 'undefined') {
  GlobalVue = window.Vue
} else if (typeof global !== 'undefined') {
  GlobalVue = global.Vue
}
if (GlobalVue) {
  GlobalVue.use(plugin)
}
