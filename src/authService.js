// This is a service class that handles the Auth0 authentication.  It will store the user's information
// in one of two places.  If a Vuex store is set, it will use the Vuex store assuming that it has the
// correct states and if no Vuex store is set, it will use LocalStorage.

// Import the event emitter library as this service will be emitting events related to logins
import EventEmitter from 'events'

// Import the Auth0 library core used for managing logins
import Auth0 from 'auth0-js'

// Import the Auth0 library for managing logins on mobile devices that use Cordova
import Auth0Cordova from '@auth0/cordova'

// Create the class definition
class AuthService extends EventEmitter {
  // The name of the key for local storage (if used)
  LOCAL_AUTHENTICATED_KEY = 'AUTHENTICATED'

  // The name of the event that is emitted by this service to publish login information
  LOGIN_EVENT = 'LOGIN_EVENT'

  // This is the Auth0 web authenticator
  webAuth = null

  // This is the Vuex store placeholder in case the wants to use Vuex to store the user state instead
  // of LocalStorage
  store = null

  // This is the name space to use when interacting with the user portion of the Vuex store
  userNamespace = null

  // Auth0 configuration properties
  domain = null
  redirectUri = null
  spaClientID = null
  mobileClientID = null
  mobilePackageID = null
  audience = null
  responseType = 'token id_token'
  scope = 'openid profile email'

  // This is a method to configure the authentication service properties
  configureService (domain, redirectUri, spaClientID, mobileClientID, mobilePackageID, audience, isMobile) {
    // TODO kgomes - validate incoming parameters

    // Set local variables
    this.domain = domain
    this.redirectUri = redirectUri
    this.spaClientID = spaClientID
    this.mobileClientID = mobileClientID
    this.mobilePackageID = mobilePackageID
    this.audience = audience

    // Create the Auth0 web authencation client that is used in multiple places
    this.webAuth = new Auth0.WebAuth({
      domain: this.domain,
      redirectUri: this.redirectUri,
      clientID: this.spaClientID,
      responseType: this.responseType,
      audience: this.audience,
      scope: this.scope
    })

    // Set the is mobile variable
    this.isMobile = isMobile

    // If the platform is mobile, override redirect handler
    if (isMobile) {
      console.log('This is on mobile so setting up redirect override')
      document.addEventListener('deviceready', function () {
        window.handleOpenURL = function (url) {
          Auth0Cordova.onRedirectUri(url)
        }
      })
    } else {
      console.log('Not on mobile')
    }
  }

  // This method allows a client to set the Vuex store that is to be used to track the user's information. Note
  // that this method also clears any local storage
  setStore (store, namespace) {
    console.log('Setting the Vuex store with namespace ' + namespace)
    this.store = store
    this.namespace = namespace

    // Try to load any properties from the local storage
    this.setStoreFromLocal()
  }

  // This method takes the information in the Vuex store (if available) and writes it to local storage to
  // persist over time
  persistStoreToLocal () {
    if (this.store) {
      localStorage.setItem(this.LOCAL_AUTHENTICATED_KEY, this.store.getters[this.namespace + '/authenticated'] || false)
      localStorage.setItem('access_token', this.store.getters[this.namespace + '/accessToken'])
      localStorage.setItem('access_token_expiry', this.store.getters[this.namespace + '/accessTokenExpiry'])
      localStorage.setItem('id_token', this.store.getters[this.namespace + '/idToken'])
      localStorage.setItem('id_token_expiry', this.store.getters[this.namespace + '/idTokenExpiry'])
      localStorage.setItem('email', this.store.getters[this.namespace + '/email'])
      localStorage.setItem('nickname', this.store.getters[this.namespace + '/nickname'])
      localStorage.setItem('name', this.store.getters[this.namespace + '/name'])
      localStorage.setItem('given_name', this.store.getters[this.namespace + '/givenName'])
      localStorage.setItem('family_name', this.store.getters[this.namespace + '/familyName'])
      localStorage.setItem('user_id', this.store.getters[this.namespace + '/userID'])
      localStorage.setItem('picture', this.store.getters[this.namespace + '/picture'])
    }
  }

  // This method looks for data in the local storage and then populates the Vuex store with that information
  setStoreFromLocal () {
    if (this.store) {
      if (localStorage.getItem(this.LOCAL_AUTHENTICATED_KEY)) {
        this.store.dispatch(this.namespace + '/authenticated', { authenticated: localStorage.getItem(this.LOCAL_AUTHENTICATED_KEY) })
        this.store.dispatch(this.namespace + '/accessToken', { accessToken: localStorage.getItem('access_token') })
        this.store.dispatch(this.namespace + '/accessTokenExpiry', { accessTokenExpiry: localStorage.getItem('access_token_expiry') })
        this.store.dispatch(this.namespace + '/idToken', { idToken: localStorage.getItem('id_token') })
        this.store.dispatch(this.namespace + '/idTokenExpiry', { idTokenExpiry: localStorage.getItem('id_token_expiry') })
        this.store.dispatch(this.namespace + '/email', { email: localStorage.getItem('email') })
        this.store.dispatch(this.namespace + '/nickname', { nickname: localStorage.getItem('nickname') })
        this.store.dispatch(this.namespace + '/name', { name: localStorage.getItem('name') })
        this.store.dispatch(this.namespace + '/givenName', { givenName: localStorage.getItem('given_name') })
        this.store.dispatch(this.namespace + '/familyName', { familyName: localStorage.getItem('family_name') })
        this.store.dispatch(this.namespace + '/userID', { userID: localStorage.getItem('user_id') })
        this.store.dispatch(this.namespace + '/picture', { picture: localStorage.getItem('picture') })
      } else {
        this.clearStore()
      }
    }
  }

  // This method clears the user information out of the Vuex store
  clearStore () {
    // Make sure we have a store first
    if (this.store) {
      this.store.dispatch(this.namespace + '/authenticated', { authenticated: false })
      this.store.dispatch(this.namespace + '/accessToken', { accessToken: null })
      this.store.dispatch(this.namespace + '/accessTokenExpiry', { accessTokenExpiry: null })
      this.store.dispatch(this.namespace + '/idToken', { idToken: null })
      this.store.dispatch(this.namespace + '/idTokenExpiry', { idTokenExpiry: null })
      this.store.dispatch(this.namespace + '/email', { email: null })
      this.store.dispatch(this.namespace + '/nickname', { nickname: null })
      this.store.dispatch(this.namespace + '/name', { name: null })
      this.store.dispatch(this.namespace + '/givenName', { givenName: null })
      this.store.dispatch(this.namespace + '/familyName', { familyName: null })
      this.store.dispatch(this.namespace + '/userID', { userID: null })
      this.store.dispatch(this.namespace + '/picture', { picture: null })
    }
  }

  // This method clears the local storage of any user information and sets the user authenticated flag to false
  clearLocalStorage () {
    localStorage.removeItem(this.LOCAL_AUTHENTICATED_KEY)
    localStorage.removeItem('access_token')
    localStorage.removeItem('access_token_expiry')
    localStorage.removeItem('id_token')
    localStorage.removeItem('id_token_expiry')
    localStorage.removeItem('email')
    localStorage.removeItem('nickname')
    localStorage.removeItem('name')
    localStorage.removeItem('given_name')
    localStorage.removeItem('family_name')
    localStorage.removeItem('user_id')
    localStorage.removeItem('picture')
  }

  // This method can be called to tell the AuthService to emit a message with the current login status
  publishStatus () {
    // Create the object that will be published
    let event = {
      authenticated: false
    }

    // First we need to decide if we are using a Vuex store, or local storage
    if (this.store) {
      // Attach the user status from the store
      event.authenticated = this.store.getters[this.namespace + '/authenticated'] || false
      if (event.authenticated) {
        event.profile = {
          accessToken: this.store.getters[this.namespace + '/accessToken'],
          accessTokenExpiry: this.store.getters[this.namespace + '/accessTokenExpiry'],
          idToken: this.store.getters[this.namespace + '/idToken'],
          idTokenExpiry: this.store.getters[this.namespace + '/idTokenExpiry'],
          email: this.store.getters[this.namespace + '/email'],
          nickname: this.store.getters[this.namespace + '/nickname'],
          name: this.store.getters[this.namespace + '/name'],
          'given_name': this.store.getters[this.namespace + '/givenName'],
          'family_name': this.store.getters[this.namespace + '/familyName'],
          'user_id': this.store.getters[this.namespace + '/userID'],
          picture: this.store.getters[this.namespace + '/picture']
        }
      }
    } else {
      event.authenticated = localStorage.getItem(this.LOCAL_AUTHENTICATED_KEY) || false
      if (event.authenticated) {
        event.profile = {
          accessToken: localStorage.getItem('access_token'),
          accessTokenExpiry: localStorage.getItem('access_token_expiry'),
          idToken: localStorage.getItem('id_token'),
          idTokenExpiry: localStorage.getItem('id_token_expiry'),
          email: localStorage.getItem('email'),
          nickname: localStorage.getItem('nickname'),
          name: localStorage.getItem('name'),
          'given_name': localStorage.getItem('given_name'),
          'family_name': localStorage.getItem('family_name'),
          'user_id': localStorage.getItem('user_id'),
          picture: localStorage.getItem('picture')
        }
      }
    }

    // Publish the event
    this.emit(this.LOGIN_EVENT, event)
  }

  // This method starts the user login flow and will do something different depending on if it's a mobile device or not
  login (customState) {
    console.log('login on AS called. isMobile? ' + this.isMobile)
    // Check to see if the application is running on a mobile device
    if (this.isMobile) {
      console.log('login is called on a mobile device')
      // Create the Auth0Cordova object
      var cordovaAuth = new Auth0Cordova({
        domain: this.domain,
        clientId: this.mobileClientID,
        packageIdentifier: this.mobilePackageID
      })
      console.log('After Auth0Cordova creation')

      // Create options for login using auth config properties
      let options = {
        scope: this.scope,
        audience: this.audience
      }

      // Grab a handle to this for callbacks
      let self = this

      // Call the method to authorize the mobile user
      cordovaAuth.authorize(options, function (err, authResult) {
        if (err) {
          console.log('Authorize returned an error:')
          console.log(err)
        } else {
          console.log('Through authorzie')
          // With the mobile login, you need to call Auth0 again to get the user profile info, so first
          // create the Auth0.Authentication object to allow us to make that call
          var auth0 = new Auth0.Authentication({
            domain: self.domain,
            clientID: self.mobileClientID
          })

          // Make the call to get the user's details using the access token that was returned from authorization
          auth0.userInfo(authResult.accessToken, function (err, profile) {
            // Check for any errors first
            if (err) {
              console.log('Err trying to get user info')
              console.log(err)
            } else {
              // The returned information should be the user's profile, so let's attach it to the original
              // response to complete the information
              authResult.idTokenPayload = profile

              // Now call the method to log the user's info into the application locally
              self.processAuthResult(authResult)
            }
          })
        }
      })
    } else {
      // Since this is not the mobile version of the application, use the WebAuth object to login. After login, the
      // user will be sent to the defined callback component which should call the handleAuthentication method.
      this.webAuth.authorize({
        appState: customState
      })
    }
  }

  // This method can be called by a component that is serving as a callback from Auth0.  When the callback component
  // calls this method, it uses the WebAuth.parseHash method to extract the user login information.
  handleAuthentication () {
    return new Promise((resolve, reject) => {
      this.webAuth.parseHash((err, authResult) => {
        if (err) {
          console.log('Could not parse authResult from hash, will reject promise')
          console.log(err)
          reject(err)
        } else {
          this.processAuthResult(authResult)
          resolve(authResult.idToken)
        }
      })
    })
  }

  // A method to handle the authentication result
  processAuthResult (authResult) {
    // Look for an access token
    if (authResult.accessToken != null) {
      // Check if we are using Vuex store or localStorage and set the authenticated flag and the access token
      if (this.store) {
        this.store.dispatch(this.namespace + '/authenticated', { authenticated: true })
        this.store.dispatch(this.namespace + '/accessToken', { accessToken: authResult.accessToken })
      } else {
        localStorage.setItem(this.LOCAL_AUTHENTICATED_KEY, true)
        localStorage.setItem('access_token', authResult.accessToken)
      }

      // Now fill out the other information if it's avaiable in the authResult
      if (authResult.idToken != null) {
        if (this.store) {
          this.store.dispatch(this.namespace + '/idToken', { idToken: authResult.idToken })
        } else {
          localStorage.setItem('id_token', authResult.idToken)
        }
      }

      // If there is a expiresIn, calculate an expiry time for the ID token
      if (authResult.expiresIn != null) {
        // Grab the current date
        let currentDate = new Date()

        // Convert the expires in to milliseconds
        let expiresInMillis = authResult.expiresIn * 1000

        // Create the timestamp in millis of the expiry
        let expiryInMillis = currentDate.getTime() + expiresInMillis

        // Store it
        if (this.store) {
          this.store.dispatch(this.namespace + '/idTokenExpiry', { idTokenExpiry: expiryInMillis })
        } else {
          // Write it to local storage
          localStorage.setItem('id_token_expiry', expiryInMillis)
        }
      }

      // Make sure we have the ID Token payload before digging in to look for more information
      if (authResult.idTokenPayload != null) {
        // Look for email
        if (authResult.idTokenPayload.email != null) {
          if (this.store) {
            this.store.dispatch(this.namespace + '/email', { email: authResult.idTokenPayload.email })
          } else {
            localStorage.setItem('email', authResult.idTokenPayload.email)
          }
        }

        // Look for nickname
        if (authResult.idTokenPayload.nickname != null) {
          if (this.store) {
            this.store.dispatch(this.namespace + '/nickname', { nickname: authResult.idTokenPayload.nickname })
          } else {
            localStorage.setItem('nickname', authResult.idTokenPayload.nickname)
          }
        }

        // Look for name
        if (authResult.idTokenPayload.name != null) {
          if (this.store) {
            this.store.dispatch(this.namespace + '/name', { name: authResult.idTokenPayload.name })
          } else {
            localStorage.setItem('name', authResult.idTokenPayload.name)
          }
        }

        // Look for given name
        if (authResult.idTokenPayload['given_name'] != null) {
          if (this.store) {
            this.store.dispatch(this.namespace + '/givenName', { givenName: authResult.idTokenPayload['given_name'] })
          } else {
            localStorage.setItem('given_name', authResult.idTokenPayload['given_name'])
          }
        }

        // Look for family name
        if (authResult.idTokenPayload['family_name'] != null) {
          if (this.store) {
            this.store.dispatch(this.namespace + '/familyName', { familyName: authResult.idTokenPayload['family_name'] })
          } else {
            localStorage.setItem('family_name', authResult.idTokenPayload['family_name'])
          }
        }

        // Look for user ID or sub
        if (authResult.idTokenPayload['user_id'] != null) {
          if (this.store) {
            this.store.dispatch(this.namespace + '/userID', { userID: authResult.idTokenPayload['user_id'] })
          } else {
            localStorage.setItem('user_id', authResult.idTokenPayload['user_id'])
          }
        }
        if (authResult.idTokenPayload['sub'] != null) {
          if (this.store) {
            this.store.dispatch(this.namespace + '/userID', { userID: authResult.idTokenPayload['sub'] })
          } else {
            localStorage.setItem('user_id', authResult.idTokenPayload['sub'])
          }
        }

        // Look for URL to picture
        if (authResult.idTokenPayload.picture != null) {
          if (this.store) {
            this.store.dispatch(this.namespace + '/picture', { picture: authResult.idTokenPayload.picture })
          } else {
            localStorage.setItem('picture', authResult.idTokenPayload.picture)
          }
        }

        // Look for an access token expiration epoch seconds
        if (authResult.idTokenPayload.exp != null) {
          if (this.store) {
            this.store.dispatch(this.namespace + '/accessTokenExpiry', { accessTokenExpiry: authResult.idTokenPayload.exp * 1000 })
          } else {
            localStorage.setItem('access_token_expiry', authResult.idTokenPayload.exp * 1000)
          }
        }
      }
    } else {
      console.log('No access token was found, so user not logged in')
      console.log(authResult)
    }

    // If we are using a Vuex store, persist it to local storage
    this.persistStoreToLocal()

    // Publish the status
    this.publishStatus()
  }

  // This is a method that returns a boolean indicating if the user is authenticated or not
  isAuthenticated () {
    // Check local storage for the authenticated key
    if (localStorage.getItem(this.LOCAL_AUTHENTICATED_KEY)) {
      return true
    } else {
      return false
    }
  }

  // This method returns and object with the user's information attached as properties
  getUserInfo () {
    if (localStorage.getItem(this.LOCAL_AUTHENTICATED_KEY)) {
      var userInfoToReturn = {
        given_name: localStorage.getItem('given_name'),
        family_name: localStorage.getItem('family_name'),
        email: localStorage.getItem('email'),
        picture: localStorage.getItem('picture'),
        name: localStorage.getItem('name')
      }
      return userInfoToReturn
    } else {
      return {}
    }
  }
  // The method that can be called to clear the user information and 'logout' so to speak
  logout () {
    // Check to see if we are using a Vuex store
    if (this.store) {
      // Clear the Vuex store
      this.clearStore()
    }
    // Clear local storage just in case
    this.clearLocalStorage()

    // If we are not on a mobile device, call logout on web auth
    if (!this.isMobile) {
      this.webAuth.logout({
        returnTo: window.location.origin,
        clientID: this.spaClientID
      })
    }

    // Now publish the login status
    this.publishStatus()
  }
}

export default new AuthService()
