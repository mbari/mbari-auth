# @mbari/mbari-auth

[![npm](https://img.shields.io/npm/v/mbari-auth.svg) ![npm](https://img.shields.io/npm/dm/mbari-auth.svg)](https://www.npmjs.com/package/mbari-auth)
[![vue2](https://img.shields.io/badge/vue-2.x-brightgreen.svg)](https://vuejs.org/)

This NPM module is a VueJS plugin that provides an authentication service utilizing the Auth0 provider.  It can be used in browser applications as well as in mobile applications.  In order to use this plugin, you must have an Auth0 account and the ability to create 'Applications' in Auth0. These instructions assume that the user will want a consistent authentication process between both application that is running in a web browser and the same application running on a mobile device via Cordova.  This allows the developer to use one consistent code base to deliver the application to both the browser and mobile devices.  A framework like Quasar can be helpful in achieving this.

These instructions do not go into details about Auth0 as they have lots of good documentation on how to use their services.  This plugin is basically a wrapper around two libraries, one for using Auth0 in a browser alone and one for using Auth0 in a mobile app using Cordova.  It simplifies the storage of the user information by using local storage and can either use a Vuex store provided by your application or you can add handlers to your components to listen for login/logout events if you don't want to use Vuex.

This plugin utilizes the auth0-js and @auth0/cordova libraries to interact with Auth0 and it is helpful if you read the documentation for those services here:

- [auth0-js](https://auth0.com/docs/libraries/auth0js/v9)
- [auth0-js API Docs](https://auth0.github.io/auth0.js/index.html)
- [@auth0/cordova npm module](https://www.npmjs.com/package/@auth0/cordova)
- [@auth0/cordova API docs](https://auth0.github.io/auth0-cordova/)
- [@auth0/cordova tutorial](https://auth0.com/docs/quickstart/native/cordova/01-login)

## Table of contents

- [Installation](#installation)
- [Prerequisites](#prerequisites)
- [Usage](#usage)
- [Example](#example)

## Prerequisites

While beyond the scope of these instructions, you will need to create both a Single Page Application and a Native Application in Auth0.

## Installation

This module is installed in the standard NPM way by using:

```bash
npm install --save @mbari/mbari-auth
```

## Usage

There are a couple of ways to use the AuthService in your application. You can use a Vuex store that has the correct state attributes to hold the user's information, or you can let the AuthService use Local Storage in the browser to hold the user's information (by the way, if you use a Vuex store, it will still be stored in local storage so it can hold user information between browser sessions).  If you use a Vuex store, you can bind the Vuex attributes in your application to make data binding easier.  If you want the AuthService without Vuex, you will need to add some methods to your components to handle event publications.  The AuthService will publish user information events if it is not using Vuex and your application will be responsible for handling those events and extracting the information from the events into data properties that are bound to your components.

## Preparation

In either usage case, you will need to create an 'application' in Auth0 to get the information necessary for this plugin to work.  In Auth0, you need to create applications for both the Single Page Application (SPA) and the mobile application (if necessary).  The authentication mechanisms behind the scenes are a bit different in the two cases, so you need different application information for Auth0 if you are using both browser and mobile instances of your application.

1. Single Page Application (SPA)
    1. Log into your Auth0 account
    1. Click on 'Applications' item on the left side menu
    1. Click on the '+ Create Application' button
    1. Enter the name of the application you are creating, for example 'My App SPA'
    1. Click on the 'Single Page Web Applications' panel, then click on 'Create' button.
    1. Click on the 'Settings' tab in your application and copy your 'Domain' and 'Client ID' for future reference.
    1. You need to identify the URLs where the application will be served from (usually something like [http://localhost:8080](http://localhost:8080)) and what callback URLs are allowed for you SPA (in development it's usually something like [http://localhost:8080/callback](http://localhost:8080/callback)).  Under the "Allowed Callback URLs" enter a comma separated list of callbacks that can be expected from the application.
    1. Under the "Allowed Web Origins", enter a comma separted list of URLs that might be hosting the application.
    1. Under the allowed Logout URLs, add a comma separated lists of possible logout URLs.  This is likely the same list as the Allowed Web Origins, but with a #/ added at the end or each URL

If you are building a mobile application, somewhere in your development process, you came up with a 'package id' for the application, also have that handy for the next step.

1. Mobile Application
    1. Click on 'Applications' item on the left side menu
    1. Click on the '+ Create Application' button
    1. Enter the name of the application you are creating, for example 'My App Mobile'
    1. Click on the 'Native' panel, then click on 'Create' button.
    1. Click on the 'Settings' tab in your application and copy your 'Domain' and 'Client ID' for future reference.
    1. Under Settings in the Allowed Callback URLs text box, enter the following with your information substituted:

    ```bash
    YOUR_APP_PACKAGE_NAME://YOUR_DOMAIN/cordova/YOUR_APP_PACKAGE_NAME/callback
    ```

    1. In the 'Allowed Origins (CORS)' section, you will need to put the URLs where the user will be sent back to your application.  It's usually something like: ```file://*, http://YOUR_IP_ADDRESS:PORT```.

At the end of these steps, you should have a domain name and a client ID for each the SPA and mobile applications.

You will most likely be using the authenticated user to call some kind of API using the tokens from this authentication plugin.  In Auth0, create an API (consult [this site](https://auth0.com/docs/quickstart/backend) for more information) and then you should have an API 'identifier' which is the 'audience' that you will need for the authentication plugin.

If you are using this plugin in a SPA, you will need to provide a callback component and URL so your application can deal with the returns from Auth0.  For example, you can create a component that looks like:

```javascript
<template>
  <div>
    <p>Loading ...</p>
  </div>
</template>

<script>
export default {
  created () {
    console.log('Callback created called')
    this.$auth.handleAuthentication()
    this.$router.push('/')
  },
  destroyed () {
    console.log('Callback destroyed called')
  }
}
</script>

<style scoped>
</style>
```

This component will be the handler for the callback URL you register with the SPA application in Auth0 so that it knows where to send the calling browser after authentication occurs.  Once the component file is created you need to add the component to the Vue router with something like:

```javascript
import Callback from '../components/Callback'

const routes = [
  {
    path: '/callback',
    name: 'callback',
    component: Callback
  }
]
```

## Example with Vuex

If you are using this plugin with Vuex, you will need to supply the Vuex store for the plug to use.  This is necessary because you will want to bind your application components to the user store so your application should be responsible for supplying the Vuex store to the plugin.  There are a minimum set of properties your user store should have.  Here is an example set of files that support these minimums:

### index.js

```javascript
import state from './state'
import * as getters from './getters'
import * as mutations from './mutations'
import * as actions from './actions'

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
```

### state.js

```javascript
// Export the state object for the store to use
export default {
  authenticated: false,
  accessToken: '',
  accessTokenExpiry: '',
  idToken: '',
  idTokenExpiry: '',
  email: '',
  nickname: '',
  name: '',
  givenName: '',
  familyName: '',
  userID: '',
  picture: ''
}
```

### getters.js

```javascript
// The getter functions for the user state
export function authenticated (state) {
  return state.authenticated
}
export function accessToken (state) {
  return state.accessToken
}
export function accessTokenExpiry (state) {
  return state.accessTokenExpiry
}
export function idToken (state) {
  return state.idToken
}
export function idTokenExpiry (state) {
  return state.idTokenExpiry
}
export function email (state) {
  return state.email
}
export function nickname (state) {
  return state.nickname
}
export function name (state) {
  return state.name
}
export function givenName (state) {
  return state.givenName
}
export function familyName (state) {
  return state.familyName
}
export function userID (state) {
  return state.userID
}
export function picture (state) {
  return state.picture
}
```

### mutations.js

```javascript
// The mutation functions
export function authenticated (state, payload) {
  state.authenticated = payload.authenticated
}
export function accessToken (state, payload) {
  state.accessToken = payload.accessToken
}
export function accessTokenExpiry (state, payload) {
  state.accessTokenExpiry = payload.accessTokenExpiry
}
export function idToken (state, payload) {
  state.idToken = payload.idToken
}
export function idTokenExpiry (state, payload) {
  state.idTokenExpiry = payload.idTokenExpiry
}
export function email (state, payload) {
  state.email = payload.email
}
export function nickname (state, payload) {
  state.nickname = payload.nickname
}
export function name (state, payload) {
  state.name = payload.name
}
export function givenName (state, payload) {
  state.givenName = payload.givenName
}
export function familyName (state, payload) {
  state.familyName = payload.familyName
}
export function userID (state, payload) {
  state.userID = payload.userID
}
export function picture (state, payload) {
  state.picture = payload.picture
}
```

### actions.js

```javascript
// The action methods
export function authenticated (context, payload) {
  context.commit('authenticated', payload)
}
export function accessToken (context, payload) {
  context.commit('accessToken', payload)
}
export function accessTokenExpiry (context, payload) {
  context.commit('accessTokenExpiry', payload)
}
export function idToken (context, payload) {
  context.commit('idToken', payload)
}
export function idTokenExpiry (context, payload) {
  context.commit('idTokenExpiry', payload)
}
export function email (context, payload) {
  context.commit('email', payload)
}
export function nickname (context, payload) {
  context.commit('nickname', payload)
}
export function name (context, payload) {
  context.commit('name', payload)
}
export function givenName (context, payload) {
  context.commit('givenName', payload)
}
export function familyName (context, payload) {
  context.commit('familyName', payload)
}
export function userID (context, payload) {
  context.commit('userID', payload)
}
export function picture (context, payload) {
  context.commit('picture', payload)
}
```

Once these files have been defined (in a directory named 'user' in this example), at the user directory level, you create a Vuex store and add the user module as follows:

```javascript
// Import Vue and Vuex
import Vue from 'vue'
import Vuex from 'vuex'

// Import the user module
import user from './user'

// Tell Vue to use Vuex
Vue.use(Vuex)

export default function () {
  const Store = new Vuex.Store({
    modules: {
      user
    }
  })

  return Store
}
```

Now, you need to import the mbari-auth npm module into your project by running

```bash
npm install --save @mbari/mbari-auth
```

In a top-level component (for example, if using a Quasar app, this might go in App.vue).

```javascript
// Grab the Vue instance
import Vue from 'vue'

// Create an instance of the AuthService
import AuthService from '@mbari/mbari-auth'

const authOptions = {
  domain: 'your.domain.name',
  redirectUri: `${window.location.origin}/callback`,
  spaClientID: 'your-spa-client-id',
  mobileClientID: 'your-mobile-client-id',
  mobilePackageID: 'your-mobile-package-id',
  audience: 'https://your.api.audience.url',
  isMobile: true|false
}

// Add the authentication service as a plugin
Vue.use(AuthService, authOptions)

// Export the Vue component
export default {
  name: 'App',
  created () {
    // After the App is created, we can be sure that the Vuex store is instantiated as well, set
    // the store on the AuthService so it can be used to store user information
    this.$auth.setStore(this.$store, 'user')
  }
}
```

Note the isMobile property is usually something you determine at run time.  For example, in Quasar you can find that by calling:

```javascript
Platform.is.mobile
```

Also, you most likely will not hard-code your id's and such into your application, you should probably use some other mechanism.  For example, in Quasar you can inject them via process.env in the quasar.conf.js file.

Now, everything should be in place to handle authentication, you now need to attach actions somewhere that can trigger the login/logout flow.  While this is not how you would want to do it in a real application, you can add two links to the page to call methods on your component.  For example if you add these:

```html
<a href="#" @click.prevent="login">
    <q-icon name="account_box" style="font-size: 2em"></q-icon>
</a>
<a href="#" @click.prevent="logout">
    <q-avatar size="24px" color="orange">LO</q-avatar>
</a>
```

with the following methods:

```javascript
    login () {
      console.log('login clicked')
      this.$auth.login()
    },
    logout () {
      this.$auth.logout()
    }
```

When the user click on the first link, they should be directed to an Auth0 login.

IMPORTANT NOTE: For this to work correctly, Vue router must be operating in ['history'](https://router.vuejs.org/guide/essentials/history-mode.html) mode otherwise all the URLs will have the '#' symbol at the start which will mess everything up.

## Usage without Vuex

> TODO

## Plugin Development

### Dev Installation

The first time you create or clone your plugin, you need to install the default dependencies:

```bash
npm install
```

### Watch and compile

This will run webpack in watching mode and output the compiled files in the `dist` folder.

```bash
npm run dev
```

### Use it in another project

If using with Cordova, you will need to run:

```bash
cordova plugin add cordova-plugin-ionic-webview
cordova plugin add cordova-plugin-safariviewcontroller
cordova plugin add cordova-plugin-customurlscheme --variable URL_SCHEME={application package name} --variable ANDROID_SCHEME={application package name} --variable ANDROID_HOST={auth0 domain} --variable ANDROID_PATHPREFIX=/cordova/{application package name}/callback
```

in the src-cordova directory so that you ensure Auth0 has access to the SafariViewController which it needs.  This is regardless of what WebView is being used.

While developing, you can follow the install instructions of your plugin and link it into the project that uses it.

In the plugin folder:

```bash
npm link
```

In the other project folder:

```bash
npm link @mbari/mbari-auth
```

This will install it in the dependencies as a symlink, so that it gets any modifications made to the plugin.

### Publish to npm

You may have to login to npm before, with `npm adduser`. The plugin will be built in production mode before getting published on npm.

```bash
npm publish --access public
```

### Manual build

This will build the plugin into the `dist` folder in production mode.

```bash
npm run build
```

---

## License

[MIT](http://opensource.org/licenses/MIT)
